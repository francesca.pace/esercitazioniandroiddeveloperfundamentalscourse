package com.example.helloword;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*        setContentView(R.layout.activity_main);
        //
        ConstraintLayout view = (ConstraintLayout)findViewById(R.id.view);
        // creo un nuovo oggetto TextView
        TextView newTextView = new TextView(this);
        // assegno un valore con setText
        newTextView.setText("Ciao a tutti");
        // aggiungo l'oggetto newTextView alla ConstraintLayout con id view
        view.addView(newTextView);
        // assegnamo un id all'oggetto newTextView
        newTextView.setId(View.generateViewId());
        // definiamo le Constraint del nuovo oggetto
        ConstraintSet set = new ConstraintSet();
        set.clone(view);
        set.connect(newTextView.getId(), ConstraintSet.TOP, view.getId(), ConstraintSet.TOP, 60);
        set.connect(newTextView.getId(), ConstraintSet.LEFT, view.getId(), ConstraintSet.LEFT, 60);
        set.connect(newTextView.getId(), ConstraintSet.RIGHT, view.getId(), ConstraintSet.RIGHT, 60);
        set.applyTo(view);*/
    }

    // definiamo il metodo che viene invocato al click sul button di nome button3
    public void onClick(View v) {
        TextView t = (TextView)findViewById(R.id.messaggio2);
        EditText num1 = (EditText)findViewById(R.id.num1);
        EditText num2 = (EditText)findViewById(R.id.num2);
        Integer msg = (Integer.valueOf(num1.getText().toString()) +  Integer.valueOf(num2.getText().toString())) ;
        t.setText(msg.toString());
        Log.e("My inputext is: ", msg.toString());
    }
}