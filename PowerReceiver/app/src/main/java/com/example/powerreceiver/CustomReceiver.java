package com.example.powerreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class CustomReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String intentAction = intent.getAction();
        if(intentAction != null) {
            switch (intentAction) {
                case Intent.ACTION_POWER_CONNECTED:
                    dispayToast(context, "Power connected");
                    break;
                case Intent.ACTION_POWER_DISCONNECTED:
                    dispayToast(context, "Power disconnected");
                    break;
                case Intent.ACTION_AIRPLANE_MODE_CHANGED:
                    dispayToast(context, "AirPlane changed");
                    break;
                case "ACTION_CUSTOM_BROADCAST":
                    dispayToast(context, "custom broadcast");
                    break;

            }
        }
    }

   private void dispayToast(Context context, String msg) {
       Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
