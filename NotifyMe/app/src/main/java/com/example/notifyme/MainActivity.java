package com.example.notifyme;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private NotificationManager mNotifyManager;
    private NotificationReceiver mReceiver = new NotificationReceiver();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createNotificationChannel();
        registerReceiver(mReceiver, new IntentFilter("ACTION_UPDATE_NOTIFICATION"));
    }

    public void onClick(View view) {
        sendNotification();
    }

    private void createNotificationChannel() {
        mNotifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel =
                    new NotificationChannel("primary_notification_channel", "first Notification" , NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("first notification");
            mNotifyManager.createNotificationChannel(notificationChannel);
        }
    }

    private void sendNotification() {
        Intent intent = new Intent("ACTION_UPDATE_NOTIFICATION");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder = getNoticationBuilder();
        builder.addAction(R.drawable.ic_stat_name, "Update Notification", pendingIntent);
        mNotifyManager.notify(0, builder.build());
    }

    private NotificationCompat.Builder getNoticationBuilder() {
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "primary_notification_channel")
                .setContentTitle("You've been notified")
                .setContentText("this is your notification")
                .setSmallIcon(R.drawable.ic_launcher_foreground);
        return builder;
    }

}