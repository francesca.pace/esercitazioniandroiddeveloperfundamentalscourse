package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Contact> contacts;
    ContactsAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rvContact = (RecyclerView) findViewById(R.id.recyclerview);
        contacts = Contact.createContactsList(20);
        adapter = new ContactsAdapter(contacts);
        rvContact.setAdapter(adapter);
        rvContact.setLayoutManager(new LinearLayoutManager(this));
    }

    public void onAdd(View view) {
        contacts.addAll(Contact.createContactsList(5));
        adapter.notifyDataSetChanged();
    }

    public  void onAddContact(View view) {
        contacts.add(0, new Contact("Mario", "Rossi", true));
        adapter.notifyItemInserted(0);
    }

    public void onShowMessage(View view) {
        int position = (int) view.getTag();
        Contact contact = adapter.getItem(position);
        displatToast("Hai selezionato l'utente di nome " + contact.getmName());
    }

    private void displatToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}