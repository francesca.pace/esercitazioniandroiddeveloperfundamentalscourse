package com.example.recyclerview;

import java.util.ArrayList;

public class Contact {
    private String mName;
    private String mSurname;
    private boolean mOnline;

    public Contact(String mName, String mSurname, boolean mOnline) {
        this.mName = mName;
        this.mSurname = mSurname;
        this.mOnline = mOnline;
    }


    public static ArrayList<Contact> createContactsList(int numContacts) {
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        for (int i= 1; i <= numContacts; i++) {
            contacts.add(new Contact("Person " + String.valueOf(i),
                    "Surname " + String.valueOf(i), i <= numContacts /2));
        }

        return contacts;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmSurname() {
        return mSurname;
    }

    public void setmSurname(String mSurname) {
        this.mSurname = mSurname;
    }

    public boolean ismOnline() {
        return mOnline;
    }

    public void setmOnline(boolean mOnline) {
        this.mOnline = mOnline;
    }
}
