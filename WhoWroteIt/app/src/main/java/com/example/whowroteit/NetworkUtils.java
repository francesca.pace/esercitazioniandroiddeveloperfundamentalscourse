package com.example.whowroteit;

import android.net.Uri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class NetworkUtils {
    private static final String BOOK_BASE_URL = "https://www.googleapis.com/books/v1/volumes?";

    static String getBookInfo(String query) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String bookJson = null;
        Uri uri = Uri.parse(BOOK_BASE_URL).buildUpon().appendQueryParameter("q", query)
                .appendQueryParameter("maxResults", "10")
                .appendQueryParameter("printType", "books").build();
        try {
            URL requestURL = new URL(uri.toString());
            urlConnection = (HttpURLConnection) requestURL.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder builder = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null){
                builder.append(line);
                builder.append("\n");
            }

            if(builder.length() == 0){
                return null;
            }

            bookJson = builder.toString();


        } catch (IOException e) {
            e.printStackTrace();
        }
        if(urlConnection != null) {
            urlConnection.disconnect();
        }
        if(reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    return bookJson;
    }
}
