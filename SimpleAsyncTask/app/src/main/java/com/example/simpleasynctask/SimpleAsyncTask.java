package com.example.simpleasynctask;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Random;

public class SimpleAsyncTask extends AsyncTask<Void, Integer, String> {

    TextView mTextView;
    ProgressBar mProgressBar;
    int s;
    SimpleAsyncTask(TextView tv, ProgressBar pb) {
        mTextView = tv;
        mProgressBar = pb;
    }
    @Override
    protected String doInBackground(Void... voids) {
        for(int i = 0; i<= s; i++) {
            try {
                Thread.sleep(1);
                publishProgress(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Log.d("LOG", "DENTRO IL TASK");
        return "Finish Task " + s;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        mProgressBar.setProgress(values[0]);
    }
    @Override
    protected  void onPostExecute(String result) {
        mTextView.setText(result);
    }

    @Override
    protected void onPreExecute() {
        Random r = new Random();
        int n = r.nextInt(10);
        s = n * 200;
        mProgressBar.setProgress(0);
        mProgressBar.setMax(s);
    }
}
