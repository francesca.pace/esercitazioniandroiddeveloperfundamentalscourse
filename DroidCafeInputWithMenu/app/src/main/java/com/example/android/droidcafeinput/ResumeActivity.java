package com.example.android.droidcafeinput;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ResumeActivity extends AppCompatActivity {
    TextView nameView;
    TextView addressView;
    TextView phoneView;
    TextView noteView;
    TextView typePhoneView;
    TextView typeDeliveryView;
    TextView dateView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume);
        Intent intent = getIntent();
        Order order = (Order) intent.getExtras().getSerializable("order");
        nameView = (TextView)findViewById(R.id.name_text);
        addressView = (TextView)findViewById(R.id.address_text);
        phoneView = (TextView)findViewById(R.id.phone_text);
        noteView = (TextView)findViewById(R.id.note_text);
        typePhoneView = (TextView)findViewById(R.id.label_spinner);
        typeDeliveryView = (TextView)findViewById(R.id.delivery_text);
        dateView = (TextView)findViewById(R.id.date_text);

        nameView.setText(order.getName());
        addressView.setText(order.getAddress());
        phoneView.setText(order.getPhone());

        noteView.setText(order.getNote());
        typePhoneView.setText(order.getType());
        dateView.setText(order.getDate());
        Log.d("label", getLabelTypeDelivery(order.getTypeDelivery()));
        typeDeliveryView.setText(getLabelTypeDelivery(order.getTypeDelivery()));

    }

    public String getLabelTypeDelivery(int type) {
        switch(type) {
            case 1:
                return getString(
                        R.string.same_day_messenger_service);
            case 2:
                return getString(
                        R.string.next_day_ground_delivery);
            case 3:
                return getString(
                        R.string.pick_up);
            default:
                return "";
        }

    }
}