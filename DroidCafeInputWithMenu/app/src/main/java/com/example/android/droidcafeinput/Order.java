package com.example.android.droidcafeinput;

import android.widget.Switch;

import java.io.Serializable;

public class Order implements Serializable {
    private String name;
    private String address;
    private String phone;
    private String note;
    private String type;
    private String date;
    private int typeDelivery;

    public Order(String name, String address, String phone, String note, String type, int typeDelivery, String date) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.note = note;
        this.type = type;
        this.date = date;
        this.typeDelivery = typeDelivery;
    }



    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTypeDelivery() {
        return typeDelivery;
    }

    public void setTypeDelivery(int typeDelivery) {
        this.typeDelivery = typeDelivery;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
