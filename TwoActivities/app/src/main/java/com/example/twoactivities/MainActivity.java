package com.example.twoactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText textInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textInput = (EditText)findViewById(R.id.textInput);
    }

    // Intent esplicito
    public void onClick(View view) {
        Intent i1 = new Intent(this, Second.class);
        String text = textInput.getText().toString();
        i1.putExtra("text",text);
        startActivity(i1);
    }

    // Intent esplicito
    public void onClick2(View view) {
        Intent i1 = new Intent(this, Third.class);
        i1.setData(Uri.parse("http://www.google.com"));
        startActivity(i1);
    }

    // Intent implicito
    public void openUrl(View view) {
        Uri uri = Uri.parse("http://www.google.com");
        Intent i1 = new Intent(Intent.ACTION_VIEW,uri);
        startActivity(i1);
    }

    // Intent implicito
    public void openDial(View view) {
        Uri uri = Uri.parse("tel:8005551234");
        Intent i1 = new Intent(Intent.ACTION_DIAL,uri);
        startActivity(i1);
    }
}